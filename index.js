const express = require("express");
const mongoose = require("mongoose");
const Product = require("./model/Product.Model");
const ProductRoute = require('./routes/product.route')
const app = express();

app.use(express.json());
app.use(express.urlencoded({extended:false}))


//routes
app.use("/api/products", ProductRoute)
app.get("/", (req, res) => {
  res.send("Hello there");
});

mongoose
  .connect(
    "mongodb+srv://kgathuai:w2LR08YXt0nBh3JK@clusterproject1.k5mq2tk.mongodb.net/NodeApp?retryWrites=true&w=majority&appName=Clusterproject1"
  )
  .then(() => {
    console.log("Connected successfully");
    app.listen(4000, () => {
      console.log("Running on port 4000");
    });
  })
  .catch(() => {
    console.log("Unable to connect");
  });
